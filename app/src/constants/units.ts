import { UnitType } from "../types";

export enum MeasurementSystem {
    IMPERIAL = "imperial",
    METRIC = "metric",
    COMBINED_MODE = "combined",
}

export enum Unit {
    TEASPOON = "tsp",
    TABLESPOON = "Tbsp",
    CUP = "cup",
    GRAM = "g",
    KILOGRAM = "kg",
    MILLILITER = "ml",
    LITER = "l",
    OUNCE = "oz",
    POUND = "lb",
}

const commonUnits = [Unit.TEASPOON, Unit.TABLESPOON, Unit.CUP];
const metricUnits = [
    Unit.GRAM,
    Unit.KILOGRAM,
    Unit.MILLILITER,
    Unit.LITER,
    ...commonUnits,
];
const imperialUnits = [Unit.OUNCE, Unit.POUND, ...commonUnits];
const allUnits = [
    Unit.GRAM,
    Unit.KILOGRAM,
    Unit.OUNCE,
    Unit.POUND,
    Unit.MILLILITER,
    Unit.LITER,
    ...commonUnits,
];

export interface UnitInfo {
    name: Unit;
    plural?: string;
    type: UnitType;
    decimals: number;
}

export const unitsInfo: { [key in Unit]: UnitInfo } = {
    [Unit.GRAM]: {
        name: Unit.GRAM,
        type: UnitType.weight,
        decimals: 0,
    },
    [Unit.KILOGRAM]: {
        name: Unit.KILOGRAM,
        type: UnitType.weight,
        decimals: 3,
    },
    [Unit.OUNCE]: {
        name: Unit.OUNCE,
        type: UnitType.weight,
        decimals: 2,
    },
    [Unit.POUND]: {
        name: Unit.POUND,
        type: UnitType.weight,
        decimals: 2,
    },
    [Unit.MILLILITER]: {
        name: Unit.MILLILITER,
        type: UnitType.volume,
        decimals: 2,
    },
    [Unit.LITER]: {
        name: Unit.LITER,
        type: UnitType.volume,
        decimals: 3,
    },
    [Unit.TEASPOON]: {
        name: Unit.TEASPOON,
        type: UnitType.volume,
        decimals: 2,
    },
    [Unit.TABLESPOON]: {
        name: Unit.TABLESPOON,
        type: UnitType.volume,
        decimals: 2,
    },
    [Unit.CUP]: {
        name: Unit.CUP,
        plural: "cups",
        type: UnitType.volume,
        decimals: 2,
    },
};

const buildUnits = (units: Unit[]): UnitInfo[] =>
    units.map((unit) => unitsInfo[unit]);

export const units = {
    commonUnits: buildUnits(commonUnits),
    metricUnits: buildUnits(metricUnits),
    imperialUnits: buildUnits(imperialUnits),
    allUnits: buildUnits(allUnits),
};

interface UnitRatio {
    [key: string]: number;
}

export const unitsNormalised: {
    weight: UnitRatio;
    volume: UnitRatio;
} = {
    // Normalised to grams
    weight: {
        [Unit.GRAM]: 1,
        [Unit.KILOGRAM]: 1000,
        [Unit.OUNCE]: 28.35,
        [Unit.POUND]: 453.6,
    },
    // Normalised to ml
    volume: {
        [Unit.MILLILITER]: 1,
        [Unit.LITER]: 1000,
        [Unit.TEASPOON]: 5.9,
        [Unit.TABLESPOON]: 17.76,
    },
};
