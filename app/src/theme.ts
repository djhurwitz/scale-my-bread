export const colours = {
    primary: "#C87637",
    primaryL1: "#d6894f",
    primaryL2: "#e8c0a2",
    secondary: "#3789C8",
    textPrimary: "#fff",
};

export const font = {
    family: `'Ubuntu', sans-serif`,
    weights: {
        regular: 400,
        bold: 700,
    },
    textSize: "16px",
};
