export const round = (value: number, decimals: number) => {
    if (!decimals) {
        return Math.round(value);
    }

    return Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
};
