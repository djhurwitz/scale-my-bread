import slugify from "slugify";
import { Recipe } from "src/types";

export const getRecipeUrl = ({ id, name }: Recipe) =>
    `/recipe/${id}/${slugify(name)}`;
