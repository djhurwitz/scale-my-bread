import { MeasurementSystem, Unit, UnitInfo, unitsInfo } from "../constants";

export const getUnitInfo = (unit: Unit): UnitInfo => unitsInfo[unit];

export const getDefaultWeightUnit = (system: MeasurementSystem): UnitInfo => {
    if (system === MeasurementSystem.IMPERIAL) {
        return unitsInfo[Unit.OUNCE];
    }
    return unitsInfo[Unit.GRAM];
};
