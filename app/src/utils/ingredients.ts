import { Unit, UnitInfo, unitsInfo, unitsNormalised } from "../constants";
import { Ingredient, UnitType } from "../types";
import { round } from "./numbers";

export const getNormalisedWeight = (ingredient: Ingredient) =>
    ingredient.value * unitsNormalised.weight[ingredient.unit.name];

export const getNormalisedVolume = (ingredient: Ingredient) =>
    ingredient.value * unitsNormalised.volume[ingredient.unit.name];

export const getTotalWeight = (
    ingredients: Ingredient[],
    outputUnit: UnitInfo
) => {
    const normalisedTotal = ingredients
        .filter((i) => i.unit.type == UnitType.weight)
        .reduce(
            (total, ingredient) => total + getNormalisedWeight(ingredient),
            0
        );

    const convertedTotal =
        normalisedTotal / unitsNormalised.weight[outputUnit.name];

    return `${round(convertedTotal, outputUnit.decimals)}${outputUnit.name}`;
};
