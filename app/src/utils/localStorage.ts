export const setItem = (key: string, value: any) =>
    localStorage.setItem(key, JSON.stringify(value));

export const getItem = <R>(key: string): R | null => {
    const value = localStorage.getItem(key);

    if (value) return JSON.parse(value);

    return null;
};

export const removeItem = localStorage.removeItem;

export const clear = localStorage.clear;
