import { FunctionalComponent, h } from "preact";
import Recipe from "../../components/recipe";
import { useGetRecipeById } from "../../state/selectors";
import Redirect from "../../components/redirect";

interface Props {
    recipeId: string;
}

const RecipeRoute: FunctionalComponent<Props> = ({ recipeId }) => {
    const recipe = useGetRecipeById(recipeId);

    if (recipe) {
        return <Recipe recipe={recipe} />;
    }

    return <Redirect to="/" />;
};

export default RecipeRoute;
