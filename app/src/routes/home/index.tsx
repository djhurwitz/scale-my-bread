import { FunctionalComponent, h } from "preact";
import { useCallback } from "preact/hooks";
import { Recipe } from "../../types";

import RecipeForm from "../../components/form/recipe-form";
import RecipeList from "../../components/recipe-list";
import { useAppActions } from "../../state/hooks";
import { useGetRecipesOrdered } from "../../state/selectors";
import { route } from "preact-router";
import { getRecipeUrl } from "../../utils/routing";
import { ContentContainer } from "../../components/layout/styled";

const Home: FunctionalComponent = () => {
    const { addRecipe } = useAppActions();
    const recipesOrdered = useGetRecipesOrdered();

    const onSubmit = useCallback(
        (recipe: Recipe) => {
            addRecipe(recipe);
            route(getRecipeUrl(recipe));
        },
        [addRecipe]
    );

    return (
        <ContentContainer>
            <RecipeForm initialIngredient onSubmit={onSubmit} />
            <br />
            {recipesOrdered.length ? <RecipeList /> : null}
        </ContentContainer>
    );
};

export default Home;
