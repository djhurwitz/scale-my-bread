import { FunctionalComponent, h } from "preact";
import { Link } from "preact-router/match";
import { ContentContainer } from "../../components/layout/styled";

const Notfound: FunctionalComponent = () => {
    return (
        <ContentContainer>
            <h1>Error 404</h1>
            <p>That page doesn&apos;t exist.</p>
            <Link href="/">
                <h4>Back to Home</h4>
            </Link>
        </ContentContainer>
    );
};

export default Notfound;
