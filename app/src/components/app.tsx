import { FunctionalComponent, h } from "preact";
import { Route, Router } from "preact-router";

import Home from "../routes/home";
import Recipe from "../routes/recipe";
import NotFoundPage from "../routes/notfound";
import Header from "./header";
import { GlobalStyles, PageContainer } from "./styled";
import { Provider } from "../state";
import LocalstorageManager from "./localstorage-manager";

const App: FunctionalComponent = () => {
    return (
        <div id="preact_root">
            <Provider>
                {/*// @ts-expect-error - not sure */}
                <GlobalStyles />
                <Header />
                <PageContainer>
                    <Router>
                        <Route path="/" component={Home} />
                        <Route
                            path="/recipe/:recipeId/:slug"
                            component={Recipe}
                        />
                        <NotFoundPage default />
                    </Router>
                </PageContainer>
                <LocalstorageManager />
            </Provider>
        </div>
    );
};

export default App;
