import styled, { createGlobalStyle } from "styled-components";
import { colours, font } from "../theme";

export const GlobalStyles = createGlobalStyle`
    html, body {
        width: 100%;
        margin: 0;
        padding: 0;
        font-family: ${font.family};
        font-weight: ${font.weights.regular};
        font-size: ${font.textSize};
        color: #fff;
        background: ${colours.primary};
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    strong,
    h1,
    h2,
    h3,
    h4,
    h5 {
        font-weight: ${font.weights.bold};
    }

    input, textbox, button, select {
        font-family: ${font.family};
        font-weight: ${font.weights.regular};
        font-size: ${font.textSize};
        color: ${colours.textPrimary};
    }
`;

export const PageContainer = styled.div`
    padding: 15px;
`;
