import { h, Fragment, FunctionalComponent } from "preact";

import { useGetRecipes, useGetRecipesOrdered } from "../../state/selectors";
import { H2 } from "../typography/styled";
import Recipe from "./recipe";
import { RecipesContainer } from "./styled";

const RecipeList: FunctionalComponent = () => {
    const recipes = useGetRecipes();
    const recipesOrdered = useGetRecipesOrdered();

    return (
        <Fragment>
            <H2>Recipes</H2>
            <RecipesContainer>
                {recipesOrdered.map((recipeId) => (
                    <Recipe {...recipes[recipeId]} />
                ))}
            </RecipesContainer>
        </Fragment>
    );
};

export default RecipeList;
