import styled from "styled-components";

export const RecipesContainer = styled.ul`
    display: flex;
    margin: 0;
    padding: 0;

    li {
        padding: 0;
        list-style: none;
    }
`;
