import { FunctionalComponent, h } from "preact";
import { Recipe as RecipeType } from "../../../types";
import { RecipeLink, RecipeName } from "./styled";
import { getRecipeUrl } from "../../../utils/routing";

const Recipe: FunctionalComponent<RecipeType> = (recipe: RecipeType) => {
    const href = getRecipeUrl(recipe);

    return (
        <RecipeLink href={href}>
            <RecipeName>{recipe.name}</RecipeName>
        </RecipeLink>
    );
};

export default Recipe;
