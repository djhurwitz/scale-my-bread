import styled from "styled-components";

export const RecipeLink = styled.a`
    display: block;
    color: #fff;
    padding: 10px;
    border: 2px solid #fff;
    border-radius: 5px;
    text-decoration: none;
`;

export const RecipeName = styled.span`
    font-size: 20px;
`;
