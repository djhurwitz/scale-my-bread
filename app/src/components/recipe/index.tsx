import { FunctionComponent, h } from "preact";
import { Recipe as RecipeType } from "../../types";
import { Link } from "preact-router/match";
import { ContentContainer } from "../../components/layout/styled";
import { H2 } from "../../components/typography/styled";
import { useMemo } from "preact/hooks";
import { getTotalWeight } from "../../utils/ingredients";
import { useMeasurementSystem } from "../../state/selectors";
import { getDefaultWeightUnit } from "../../utils/units";
import { Unit, unitsInfo } from "../../constants";

interface Props {
    recipe: RecipeType;
}

const Recipe: FunctionComponent<Props> = ({ recipe }: Props) => {
    const { name, ingredients, link, method } = recipe;

    const system = useMeasurementSystem();

    const totalWeight = useMemo(() => {
        const defaultUnit = getDefaultWeightUnit(system);
        return getTotalWeight(ingredients, defaultUnit);
    }, [ingredients, system]);

    return (
        <ContentContainer>
            <Link href="/">🠔 Back to recipes</Link>
            <H2>{name}</H2>
            {link && <a href={link}>Link to recipe</a>}
            <div>
                <h3>Ingredients</h3>
                <ul>
                    {ingredients.map(
                        ({ id, name: ingredientName, value, unit }) => (
                            <li key={id}>
                                {value}
                                {unit.name}
                                {"   "}
                                <strong>{ingredientName}</strong>
                            </li>
                        )
                    )}
                </ul>
                <strong>Total weight: {totalWeight}</strong>
            </div>
            {method && (
                <div>
                    <h3>Method</h3>
                    {method}
                </div>
            )}
        </ContentContainer>
    );
};

export default Recipe;
