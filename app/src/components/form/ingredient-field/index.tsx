import { FunctionalComponent, h, JSX } from "preact";
import { getUnitInfo } from "../../../utils/units";
import { Unit, UnitInfo } from "../../../constants";
import { Ingredient } from "../../../types";
import { Field, FieldGroup, Label, Select, TextField } from "../inputs/styled";
import { Button } from "../../buttons";

interface Props {
    index?: number;
    ingredient: Ingredient;
    units: UnitInfo[];
    onChange: (ingredient: Ingredient) => void;
    onAddIngredient: (index?: number) => void;
}

const IngredientField: FunctionalComponent<Props> = ({
    index,
    ingredient,
    units,
    onChange,
    onAddIngredient,
}: Props) => {
    const onNameChange = (
        event: JSX.TargetedEvent<HTMLInputElement, Event>
    ) => {
        const { value: name } = event.currentTarget;
        onChange({ ...ingredient, name });
    };

    const onValueChange = (
        event: JSX.TargetedEvent<HTMLInputElement, Event>
    ) => {
        const { value } = event.currentTarget;
        onChange({ ...ingredient, value: value ? Number(value) : 0 });
    };

    const onUnitChange = (
        event: JSX.TargetedEvent<HTMLSelectElement, Event>
    ) => {
        const { value: unit } = event.currentTarget;
        if (unit) {
            const unitInfo = getUnitInfo(unit as Unit);
            onChange({ ...ingredient, unit: unitInfo });
        }
    };

    const onAdd = () => {
        onAddIngredient(index);
    };

    return (
        <FieldGroup>
            <Field>
                <Label htmlFor="ingredient-name">Ingredient name</Label>
                <TextField
                    type="text"
                    id="ingredient-name"
                    name="ingredient-name"
                    onInput={onNameChange}
                    value={ingredient.name}
                    autoComplete="off"
                />
            </Field>
            <Field>
                <Label htmlFor="ingredient-measurement">Amount</Label>
                <TextField
                    type="text"
                    id="ingredient-measurement"
                    name="ingredient-measurement"
                    onInput={onValueChange}
                    value={ingredient.value}
                    autoComplete="off"
                />
            </Field>
            <Field>
                <Label htmlFor="ingredient-unit">Unit</Label>
                <Select
                    id="ingredient-unit"
                    name="ingredient-unit"
                    onInput={onUnitChange}
                    value={ingredient.unit.name}
                >
                    {units.map((unit) => (
                        <option key={unit.name} value={unit.name}>
                            {ingredient.value > 1 && unit.plural
                                ? unit.plural
                                : unit.name}
                        </option>
                    ))}
                </Select>
            </Field>
            <Field>
                <Button onClick={onAdd} type="button">
                    +
                </Button>
            </Field>
        </FieldGroup>
    );
};

export default IngredientField;
