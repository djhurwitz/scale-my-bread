import { colours } from "../../../theme";
import styled, { css } from "styled-components";

const sharedInputStyles = css`
    padding: 30px 10px 10px;
    border-radius: 4px;
    border: 2px solid ${colours.primaryL1};
    background: ${colours.primaryL1};
    box-shadow: inset -2px -3px 14px -3px rgb(0 0 0 / 10%);
    color: white;
    outline: none;
    &:focus {
        border-color: ${colours.primaryL2};
    }
`;

export const Label = styled.label`
    font-size: 14px;
`;

export const Field = styled.div`
    display: inline-flex;
    position: relative;
    margin-bottom: 10px;

    ${Label} {
        position: absolute;
        left: 10px;
        top: 5px;
    }
`;

export const FieldGroup = styled.div`
    ${Field} + ${Field} {
        margin-left: 10px;
    }
`;

export const TextField = styled.input`
    ${sharedInputStyles}
`;

export const TextArea = styled.textarea`
    ${sharedInputStyles}
`;

export const Select = styled.select`
    ${sharedInputStyles}
`;
