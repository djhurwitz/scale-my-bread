import { JSX } from "preact";
import { useCallback, useReducer } from "preact/hooks";
import { nanoid } from "nanoid";

import { Recipe, Ingredient } from "../../../types";
import {
    Actions,
    addIngredientAction,
    ADD_INGREDIENT,
    editIngredientAction,
    editRecipeTextFieldAction,
    EDIT_INGREDIENT,
    EDIT_RECIPE_TEXT_FIELD,
    resetRecipeFormAction,
    RESET_RECIPE_FORM,
} from "./actions";
import { Unit, unitsInfo } from "../../../constants";

interface State {
    recipe: Recipe;
}

interface UseRecipeFormParams {
    initialIngredient?: boolean;
    initialRecipe?: Recipe;
    onSubmit: (recipe: Recipe) => void;
}

function createIngredient(): Ingredient {
    return {
        id: nanoid(),
        name: "",
        unit: unitsInfo[Unit.GRAM],
        value: 0,
    };
}

function useRecipeFormReducer(state: State, action: Actions): State {
    if (action.type === EDIT_RECIPE_TEXT_FIELD) {
        const { fieldName, value } = action.payload;
        return {
            ...state,
            recipe: {
                ...state.recipe,
                [fieldName]: value,
            },
        };
    }

    if (action.type === ADD_INGREDIENT) {
        const { index } = action.payload;
        const newIngredient = createIngredient();

        const ingredients = [...state.recipe.ingredients];

        if (index !== undefined) {
            ingredients.splice(index + 1, 0, newIngredient);
        } else {
            ingredients.push(newIngredient);
        }

        return {
            ...state,
            recipe: {
                ...state.recipe,
                ingredients,
            },
        };
    }

    if (action.type === EDIT_INGREDIENT) {
        const { ingredient } = action.payload;
        const ingredientIndex = state.recipe.ingredients.findIndex(
            (i) => i.id === ingredient.id
        );

        if (ingredientIndex > -1) {
            const updatedIngredients = state.recipe.ingredients.concat();
            updatedIngredients[ingredientIndex] = { ...ingredient };
            return {
                ...state,
                recipe: {
                    ...state.recipe,
                    ingredients: updatedIngredients,
                },
            };
        }

        return state;
    }

    if (action.type === RESET_RECIPE_FORM) {
        return {
            recipe: {
                id: nanoid(12),
                name: "",
                ingredients: [],
                link: "",
                method: "",
            },
        };
    }

    return state;
}

const initReducer = ({
    initialIngredient,
    initialRecipe,
}: UseRecipeFormParams) => {
    if (initialRecipe) {
        return {
            recipe: initialRecipe,
        };
    }

    const ingredients = [];

    if (initialIngredient) {
        ingredients.push(createIngredient());
    }

    return {
        recipe: {
            id: nanoid(12),
            name: "",
            method: "",
            link: "",
            ingredients,
        },
    };
};

export const useRecipeForm = (params: UseRecipeFormParams) => {
    const { onSubmit } = params;
    const [state, dispatch] = useReducer<State, Actions, UseRecipeFormParams>(
        useRecipeFormReducer,
        params,
        initReducer
    );

    const onChangeTextField = useCallback(
        (event: JSX.TargetedEvent<HTMLInputElement | HTMLTextAreaElement>) => {
            const { name, value } = event.currentTarget;
            dispatch(editRecipeTextFieldAction(name, value));
        },
        []
    );

    const onAddIngredient = useCallback((index?: number) => {
        dispatch(addIngredientAction(index));
    }, []);

    const onChangeIngredient = useCallback((ingredient: Ingredient) => {
        dispatch(editIngredientAction(ingredient));
    }, []);

    const submitForm = useCallback(
        (event: JSX.TargetedEvent<HTMLElement>) => {
            event.preventDefault();
            onSubmit(state.recipe);
            dispatch(resetRecipeFormAction());
        },
        [onSubmit, state.recipe]
    );

    return {
        recipe: state.recipe,
        onChangeTextField,
        onAddIngredient,
        onChangeIngredient,
        submitForm,
    };
};
