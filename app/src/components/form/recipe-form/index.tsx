import { FunctionalComponent, h } from "preact";

import { useRecipeForm } from "./hooks";
import IngredientField from "../ingredient-field";
import { Recipe } from "../../../types";
import { useUnits } from "../../../state/hooks";
import { Field, Label, TextArea, TextField } from "../inputs/styled";
import { Button } from "../../../components/buttons";

interface Props {
    initialIngredient?: boolean;
    initialRecipe?: Recipe;
    onSubmit: (recipe: Recipe) => void;
}

const RecipeForm: FunctionalComponent<Props> = ({
    initialIngredient,
    initialRecipe,
    onSubmit,
}) => {
    const {
        recipe: { name, link, method, ingredients },
        onChangeTextField,
        onAddIngredient,
        onChangeIngredient,
        submitForm,
    } = useRecipeForm({
        initialIngredient,
        initialRecipe,
        onSubmit,
    });

    const units = useUnits();

    return (
        <form onSubmit={submitForm}>
            <div>
                <Field>
                    <Label htmlFor="name">Recipe name</Label>
                    <TextField
                        id="name"
                        name="name"
                        value={name}
                        onInput={onChangeTextField}
                        autoComplete="off"
                    />
                </Field>
            </div>
            <div>
                <Field>
                    <Label htmlFor="link">Link</Label>
                    <TextField
                        id="link"
                        name="link"
                        value={link}
                        onInput={onChangeTextField}
                        autoComplete="off"
                    />
                </Field>
            </div>
            {ingredients.map((ingredient, index) => (
                <div key={ingredient.id}>
                    <IngredientField
                        index={index}
                        ingredient={ingredient}
                        onChange={onChangeIngredient}
                        onAddIngredient={onAddIngredient}
                        units={units}
                    />
                </div>
            ))}
            <Field>
                <Label htmlFor="method">Method</Label>
                <TextArea
                    id="method"
                    name="method"
                    onInput={onChangeTextField}
                    value={method}
                />
            </Field>
            <div>
                <Button type="submit">Save</Button>
            </div>
        </form>
    );
};

export default RecipeForm;
