import { Ingredient } from "../../../types";

export const EDIT_RECIPE_TEXT_FIELD = "EDIT_RECIPE_TEXT_FIELD";

interface EditRecipeTextFieldAction {
    type: typeof EDIT_RECIPE_TEXT_FIELD;
    payload: {
        fieldName: string;
        value: string;
    };
}

export const editRecipeTextFieldAction = (
    fieldName: string,
    value: string
): EditRecipeTextFieldAction => ({
    type: EDIT_RECIPE_TEXT_FIELD,
    payload: { fieldName, value },
});

export const EDIT_INGREDIENT = "EDIT_INGREDIENT";

interface EditIngredientAction {
    type: typeof EDIT_INGREDIENT;
    payload: {
        ingredient: Ingredient;
    };
}

export const editIngredientAction = (
    ingredient: Ingredient
): EditIngredientAction => ({
    type: EDIT_INGREDIENT,
    payload: { ingredient },
});

export const ADD_INGREDIENT = "ADD_INGREDIENT";

interface AddIngredientAction {
    type: typeof ADD_INGREDIENT;
    payload: {
        index?: number;
    };
}

export const addIngredientAction = (index?: number): AddIngredientAction => ({
    type: ADD_INGREDIENT,
    payload: {
        index,
    },
});

export const RESET_RECIPE_FORM = "RESET_RECIPE_FORM";

interface ResetRecipeFormAction {
    type: typeof RESET_RECIPE_FORM;
}

export const resetRecipeFormAction = (): ResetRecipeFormAction => ({
    type: RESET_RECIPE_FORM,
});

export type Actions =
    | EditRecipeTextFieldAction
    | EditIngredientAction
    | AddIngredientAction
    | ResetRecipeFormAction;
