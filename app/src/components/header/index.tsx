import { FunctionalComponent, h } from "preact";
import { Link } from "preact-router/match";
import { ContentContainer, FlexRow } from "../layout/styled";

import { HeaderContainer, Icon, Title } from "./styled";

const Header: FunctionalComponent = () => {
    return (
        <HeaderContainer>
            <ContentContainer>
                <FlexRow>
                    <Icon>🍞</Icon>
                    <Title>Scale My Bread</Title>
                    <nav>
                        <Link href="/">Recipes</Link>
                    </nav>
                </FlexRow>
            </ContentContainer>
        </HeaderContainer>
    );
};

export default Header;
