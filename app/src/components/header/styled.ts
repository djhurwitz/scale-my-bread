import styled from "styled-components";
import { colours } from "../../theme";

export const HeaderContainer = styled.header`
    color: ${colours.textPrimary};
    background: ${colours.primary};
    border-bottom: 2px solid rgba(255, 255, 255, 0.2);
    box-shadow: 0 0 9px -1px rgb(0 0 0 / 20%);
    padding: 10px;
`;

export const Icon = styled.div`
    font-size: 28px;
    padding-right: 5px;
`;

export const Title = styled.h1`
    font-size: 28px;
    margin: 0 auto 0 0;
`;
