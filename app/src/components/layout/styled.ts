import styled from "styled-components";

export const ContentContainer = styled.div`
    max-width: 800px;
    padding: 0 20px;
    margin: 0 auto;
`;

export const FlexRow = styled.div`
    display: flex;
    flex-flow: row wrap;
`;
