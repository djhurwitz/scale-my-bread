import { FunctionalComponent } from "preact";
import { useEffect, useRef } from "preact/hooks";
import { RECIPES_STORAGE_KEY } from "../../constants";
import { useGetRecipes, useGetRecipesOrdered } from "../../state/selectors";

const LocalstorageManager: FunctionalComponent = () => {
    const initialised = useRef(false);
    const recipes = useGetRecipes();
    const recipesOrdered = useGetRecipesOrdered();

    useEffect(() => {
        if (initialised.current) {
            localStorage.setItem(
                RECIPES_STORAGE_KEY,
                JSON.stringify({
                    recipes,
                    recipesOrdered,
                })
            );
        }
    }, [recipes, recipesOrdered]);

    useEffect(() => {
        initialised.current = true;
    }, []);

    return null;
};

export default LocalstorageManager;
