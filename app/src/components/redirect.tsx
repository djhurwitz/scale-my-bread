import { FunctionalComponent, h } from "preact";
import { route } from "preact-router";
import { useEffect } from "preact/hooks";

interface Props {
    to: string;
    replace?: boolean;
}

const Redirect: FunctionalComponent<Props> = ({ to, replace }: Props) => {
    useEffect(() => {
        route(to, replace);
    }, []);

    return null;
};

export default Redirect;
