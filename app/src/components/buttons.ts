import { colours } from "../theme";
import styled from "styled-components";

export const Button = styled.button`
    padding: 10px 20px;
    background: ${colours.secondary};
    border-radius: 4px;
    border: 0;
    margin: 0;
    cursor: pointer;
`;
