import { UnitInfo } from "../constants";

export enum UnitType {
    weight = "weight",
    volume = "volume",
    quantity = "quantity",
}

export type IngredientID = string;

export interface Ingredient {
    id: IngredientID;
    name: string;
    unit: UnitInfo;
    value: number;
}

export interface Recipe {
    id: string;
    name: string;
    ingredients: Ingredient[];
    link?: string;
    method?: string;
}
