import { h, FunctionalComponent, createContext } from "preact";
import { useMemo, useReducer } from "preact/hooks";

import { AppState } from "./types";
import appReducer from "./reducer";
import { Actions } from "./actions";
import { MeasurementSystem, RECIPES_STORAGE_KEY } from "../constants";
import * as localStorage from "../utils/localstorage";

export interface AppContextInterFace extends AppState {
    dispatch: (action: Actions) => void;
}

export const initialContext = (): AppContextInterFace => ({
    recipes: {},
    recipesOrdered: [],
    mode: MeasurementSystem.METRIC,
    dispatch: () => {},
});

export const AppContext = createContext<AppContextInterFace>(initialContext());

export const initReducer = (): AppState => {
    const storedState = localStorage.getItem<AppState>(RECIPES_STORAGE_KEY);
    return (
        storedState ?? {
            recipes: {},
            recipesOrdered: [],
            mode: MeasurementSystem.METRIC,
        }
    );
};

export const Provider: FunctionalComponent = ({ children }) => {
    const [appState, dispatch] = useReducer<AppState, Actions, null>(
        appReducer,
        null,
        initReducer
    );

    const appContext = useMemo<AppContextInterFace>(
        () => ({
            ...appState,
            dispatch,
        }),
        [appState, dispatch]
    );

    return (
        <AppContext.Provider value={appContext}>{children}</AppContext.Provider>
    );
};
