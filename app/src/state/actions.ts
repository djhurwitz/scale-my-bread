import { Recipe } from "../types";

export const ADD_RECIPE: "ADD_RECIPE" = "ADD_RECIPE";

interface AddRecipeAction {
    type: typeof ADD_RECIPE;
    payload: {
        recipe: Recipe;
    };
}

export const addRecipeAction = (recipe: Recipe): AddRecipeAction => ({
    type: ADD_RECIPE,
    payload: { recipe },
});

export const EDIT_RECIPE: "EDIT_RECIPE" = "EDIT_RECIPE";

interface EditRecipeAction {
    type: typeof EDIT_RECIPE;
    payload: {
        recipeId: string;
    };
}

export const editRecipeAction = (recipeId: string): EditRecipeAction => ({
    type: EDIT_RECIPE,
    payload: { recipeId },
});

export const DELETE_RECIPE: "DELETE_RECIPE" = "DELETE_RECIPE";

interface DeleteRecipeAction {
    type: typeof DELETE_RECIPE;
    payload: {
        recipeId: string;
    };
}

export const deleteRecipeAction = (recipeId: string): DeleteRecipeAction => ({
    type: DELETE_RECIPE,
    payload: { recipeId },
});

export type Actions = AddRecipeAction | EditRecipeAction | DeleteRecipeAction;
