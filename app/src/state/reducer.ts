import { Actions, ADD_RECIPE, DELETE_RECIPE } from "./actions";
import { AppState } from "./types";

const appReducer = (state: AppState, action: Actions): AppState => {
    if (action.type === ADD_RECIPE) {
        const { recipe } = action.payload;
        const recipes = { ...state.recipes, [recipe.id]: recipe };
        const recipesOrdered = state.recipesOrdered.concat([recipe.id]);
        return {
            ...state,
            recipes,
            recipesOrdered,
        };
    }

    if (action.type === DELETE_RECIPE) {
        const { recipeId } = action.payload;
        const recipes = { ...state.recipes };
        const recipesOrdered = state.recipesOrdered.filter(
            (id) => id !== recipeId
        );

        delete recipes[recipeId];

        return {
            ...state,
            recipes,
            recipesOrdered,
        };
    }

    return state;
};

export default appReducer;
