import { useContext } from "preact/hooks";
import { MeasurementSystem } from "src/constants";

import { AppContext } from ".";
import { Recipe } from "../types";
import { AppState } from "./types";

const createSelector =
    <R, P = void>(
        handler: (appState: AppState, param: P) => R
    ): ((param: P) => R) =>
    (param: P) => {
        const { dispatch, ...appState } = useContext(AppContext);
        return handler(appState, param);
    };

export const useGetRecipes = createSelector<{ [key: string]: Recipe }>(
    (appState) => appState.recipes
);

export const useGetRecipesOrdered = createSelector<string[]>(
    (appState) => appState.recipesOrdered
);

export const useGetRecipeById = createSelector<Recipe, string>(
    (_, recipeId) => {
        const recipes = useGetRecipes();
        return recipes[recipeId];
    }
);

export const useMeasurementSystem = createSelector<MeasurementSystem>(
    (appState) => appState.mode
);
