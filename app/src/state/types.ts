import { MeasurementSystem } from "src/constants";
import { Recipe } from "../types";

export interface AppState {
    recipes: {
        [recipeId: string]: Recipe;
    };
    recipesOrdered: string[];
    mode: MeasurementSystem;
}
