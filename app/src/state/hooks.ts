import { useContext, useMemo } from "preact/hooks";
import { MeasurementSystem, UnitInfo, units } from "../constants";
import { AppContext } from ".";
import { Recipe } from "../types";
import {
    addRecipeAction,
    editRecipeAction,
    deleteRecipeAction,
} from "./actions";
import { useMeasurementSystem } from "./selectors";

interface AppActions {
    addRecipe: (recipe: Recipe) => void;
    editRecipe: (recipeId: string) => void;
    deleteRecipe: (recipeId: string) => void;
}

export const useAppActions = () => {
    const { dispatch } = useContext(AppContext);

    return useMemo<AppActions>(
        () => ({
            addRecipe: (recipe: Recipe) => {
                dispatch(addRecipeAction(recipe));
            },
            editRecipe: (recipeId: string) => {
                dispatch(editRecipeAction(recipeId));
            },
            deleteRecipe: (recipeId: string) => {
                dispatch(deleteRecipeAction(recipeId));
            },
        }),
        [dispatch]
    );
};

export const useUnits = (): UnitInfo[] => {
    const mode = useMeasurementSystem();
    switch (mode) {
        case MeasurementSystem.COMBINED_MODE:
            return units.allUnits;
        case MeasurementSystem.IMPERIAL:
            return units.imperialUnits;
        default:
            return units.metricUnits;
    }
};
